(function(){
    function _$(){
      return this;
    };
    
    _$.prototype = {
      car: {},
      slipAngleBase: {},
      angleSpeed: 0,
      switchPieceChecked: null,
      speedRate: 0,
      setCar: function(car) {
        this.car = car;
        return this;
      },
      calcDrag: function() {
        return ((this.car.log.speeds[1] - (this.car.log.speeds[2] - this.car.log.speeds[1])) / Math.pow(this.car.log.speeds[1], 2) * 1.0);
      },
      calcMass: function() {
        return (1.0 / ( Math.log( ( this.car.log.speeds[3] - ( 1.0 / this.car.drag ) ) / ( this.car.log.speeds[2] - ( 1.0 / this.car.drag ) ) ) / ( -this.car.drag ) ));
      },
      calcCentripetalForce: function() {
        return (Math.pow(this.car.current.speed, 2) / this.car.current.pieceRadius);
      },
      calcSpeedRate: function() {
        var difSpeed = [],
          speeds = this.car.log.speeds;
        
        difSpeed.push(speeds[speeds.length-2] - speeds[speeds.length-3]);
        difSpeed.push(speeds[speeds.length-1] - speeds[speeds.length-2]);
        
        return (difSpeed[1]*100/difSpeed[0])/100;
      },
      predictNextSpeed: function(throttle, speed, dif) {
        var speeds = this.car.log.speeds,
          difSpeed = !!dif ? dif : (speeds[speeds.length-1] - speeds[speeds.length-2]);

        if(!this.speedRate) {
          return;
        }

        if(!throttle) {
          return (speed * this.speedRate);
        } else {
          return (speed + ((difSpeed * this.speedRate) * throttle));
        }
      },
      calcSpeed: function() {
        var positions = this.car.log.positions;
        
        if(positions.length == 1) {
          return positions[0].data.piecePosition.inPieceDistance;
        }

        var lastPosition = positions[positions.length-2],
          currentPosition = positions[positions.length-1],
          pointA = {index: lastPosition.data.piecePosition.pieceIndex, distance: lastPosition.data.piecePosition.inPieceDistance},
          pointB = {index: currentPosition.data.piecePosition.pieceIndex, distance: currentPosition.data.piecePosition.inPieceDistance};
        
        return this.calcDistance(pointA, pointB, this.car.current.lane);
      },
      calcSlipAngleSpeed: function(radius) {
        var massDragRatio = (this.car.mass * this.car.drag);

        if(!this.slipAngleBase.angle) {
          return (Math.sqrt(massDragRatio * radius) - this.car.log.speeds[1]);
        }

        return Math.sqrt( ((this.slipAngleBase.force + massDragRatio)/2)  * radius);
      },
      calcDistance: function(start, end, laneIndex) {
        var distance = 0,
          trackPieces = race.config.track.pieces,
          qtRacePieces = (trackPieces.length - 1);
          _parent = this,
          fncCalcPiecesLength = function(_start, _end) {
            var _length = 0;

            for(var i = _start.index, pieceLength = 0; i <= _end.index; i++) {
              //Calc Piece Length
              var pieceLength = _parent.calcPiece(race.config.track.pieces[i], race.config.track.lanes[laneIndex]).pieceLength;
              //Calc Distance
              if(i == _start.index && i == _end.index) {
                _length += _end.distance - _start.distance;
              } else if(i == _start.index) {
                _length += (pieceLength - _start.distance);
              } else if(i == _end.index) {
                _length += _end.distance;
              } else {
                _length += pieceLength;
              }
            }
            
            return _length;
          }

        if(end.index < start.index) {
          distance = fncCalcPiecesLength(start, {index:qtRacePieces, distance:race.config.track.pieces[qtRacePieces].length});
          distance += fncCalcPiecesLength({index:0,distance:0}, {index:end.index, distance:0});
        } else {
          distance = fncCalcPiecesLength(start, end);
        }
        
        return distance;
      },
      calcPiece: function(piece, lane) {
        if(piece.hasOwnProperty("angle")) {
          var distanceFromCenterSignal = piece.angle < 0 ? -1 : 1,
            angle = piece.angle,
            radius = Math.abs(piece.radius - (lane.distanceFromCenter * distanceFromCenterSignal));

          return {
            angle: angle,          
            radius: radius,
            pieceLength: (Math.abs(angle) * (radius * (2*Math.PI)) / 360),
            maxSpeed: this.calcSlipAngleSpeed(radius)
          };
        } else {
          return {
            pieceLength: piece.length,
            maxSpeed: 0
          };
        }
      },
      nextPieceByType: function(type, laneIndex, start, end) {
        var resultIndex = 0,
          fncSearchForPiece = function(_startIndex, _endIndex) {
            for(var i = _startIndex+1; i <= _endIndex; i++) {
              if(race.config.track.pieces[i].hasOwnProperty(type)) {
                resultIndex = i;
                break;
              }
            }
          };

        fncSearchForPiece(start.index, end.index);

        if(!resultIndex) {
          fncSearchForPiece(0, end.index);
        }

        return {
          index: resultIndex,
          piece: this.calcPiece(race.config.track.pieces[resultIndex], race.config.track.lanes[laneIndex]),
          distanceTo: this.calcDistance(start, {index:resultIndex, distance:0}, laneIndex)
        };
      },
      shortestLane: function() {
        if(this.switchPieceChecked == this.car.current.pieceIndex) {
          return false;
        }

        var direction = "",
          nextSwitches = [],
          possibleLanes = [this.car.current.lane-1, this.car.current.lane, this.car.current.lane+1],
          lastRacePieceIndex = (race.config.track.pieces.length-1),
          lastDistanceChecked = 0,
          leastDistantLane = null,
          switchDirection = false;

        for(var i in race.config.track.lanes) {
          var l = possibleLanes.indexOf(race.config.track.lanes[i].index);

          if(l > -1) {
            var nextSwitch = this.nextPieceByType("switch", possibleLanes[l], {index: this.car.current.nextPieceIndex+1, distance:0}, {index:lastRacePieceIndex, distance: race.config.track.pieces[lastRacePieceIndex].length}); 
        
            if(l === 0 || l === 2) {
              nextSwitch.distanceTo += 2.05;
            }

            if(!lastDistanceChecked || nextSwitch.distanceTo < lastDistanceChecked) {
              leastDistantLane = possibleLanes[l];
            } else if(nextSwitch.distanceTo == lastDistanceChecked) {
              leastDistantLane = null;
            }

            lastDistanceChecked = nextSwitch.distanceTo;
          }
        }

        if(leastDistantLane == possibleLanes[0]) {
          switchDirection = "Left";
        } else if(leastDistantLane == possibleLanes[2]) {
          switchDirection = "Right";
        }
        
        if(switchDirection) {
          send({
            msgType: "switchLane",
            data: switchDirection
          });
        }

        this.switchPieceChecked = this.car.current.pieceIndex;
      },
      calcBestPiece4Turbo: function() {
        var pPoints = {},
          points = 0,
          ind = 0;

        for(var i in race.config.track.pieces) {
          var p = race.config.track.pieces[i];

          if(!p.hasOwnProperty("angle")) {
            points++;
          } else {
            pPoints[ind] = points;
            points = 0;
            ind = parseInt(i)+1;
          }
        }

        pPoints[ind] = points + pPoints[0];
        
        var bestPiece4Turbo = 0,
          max = 0;

        for(var i in pPoints) {
          if(pPoints[i] > max) {
            bestPiece4Turbo = i;
            max = pPoints[i];
          }
        }

        game.bestPiece4Turbo = bestPiece4Turbo;
      }
    }

    physics = function() {return new _$();};
})();

(function(){
    function _$(d){
      this.gameCar = d;

      return this;
    };
    
    _$.prototype={
      gameCar: {},
      mass: 0.0,
      drag: 0,
      turbo: {
        available: false,
        active: false
      },
      throttle: 0.0,
      before: {},
      current: {
        speed: 0.0,
        lane: 0,
        centripetalForce: 0,
        angle: 0,
        lane: 0,
        lap: 0,
        inPieceDistance: 0,
        pieceIndex: 0,
        pieceIndex: 0,
        pieceAngle: 0,
        pieceRadius: 0,
        pieceSwitch: false,
        pieceMaxSpeed: 0,
        throttle: 0
      },
      log: {
        ticks: [],
        speeds: [],
        positions: []
      },
      physics: physics(),
      ajustVars: function() {        
        var lastPosition = this.log.positions[this.log.positions.length-1];

        this.current.lane = lastPosition.data.piecePosition.lane.endLaneIndex;
        this.current.inPieceDistance = lastPosition.data.piecePosition.inPieceDistance;
        this.current.pieceIndex = lastPosition.data.piecePosition.pieceIndex;
        this.current.nextPieceIndex = this.current.pieceIndex < (race.config.track.pieces.length-1) ? (this.current.pieceIndex+1) : 0;
        this.current.angle = Math.abs(lastPosition.data.angle);
        this.current.lap = lastPosition.data.piecePosition.lap;

        var currPiece = race.config.track.pieces[this.current.pieceIndex],
          currPieceSpecs = this.physics.calcPiece(currPiece, race.config.track.lanes[this.current.lane]);

        this.current.pieceAngle = currPieceSpecs.angle;
        this.current.pieceRadius = currPieceSpecs.radius;
        this.current.pieceSwitch = currPiece.hasOwnProperty("switch") ? currPiece.switch : false;
        this.current.pieceMaxSpeed = currPieceSpecs.maxSpeed;

        this.current.centripetalForce = this.physics.setCar(this).calcCentripetalForce();

        if(!this.physics.slipAngleBase.angle && this.current.angle) {
          var FcSlip = ((this.current.speed / this.current.pieceRadius)*(180/Math.PI)) - this.current.angle,
            vThresh = FcSlip * (Math.PI/180) * this.current.pieceRadius;

          //game.log("FcSlip", FcSlip);
          //game.log("vThresh", vThresh);

          this.physics.slipAngleBase = {
            angle: this.current.angle,
            speed: this.current.speed,
            force: (Math.pow(vThresh,2) / this.current.pieceRadius)
          }

          //game.log("this.physics.slipAngleBase", this.physics.slipAngleBase);
        }
      },
      move: function() {
        this.ajustVars();
        this.current.speed = this.physics.setCar(this).calcSpeed();
        this.log.speeds.push(this.current.speed);

        var throttle = 1.0,
          lastRacePieceIndex = (race.config.track.pieces.length-1),
          nextCurve = this.physics.nextPieceByType("angle", this.current.lane, {index:this.current.pieceIndex, distance: this.current.inPieceDistance}, {index:lastRacePieceIndex, distance: race.config.track.pieces[lastRacePieceIndex].length});

        if(this.physics.slipAngleBase.force && race.config.track.pieces[this.current.nextPieceIndex].hasOwnProperty("switch") && race.config.track.pieces[this.current.nextPieceIndex].switch) {
          this.physics.setCar(this).shortestLane();
        }

        
        if(this.current.pieceAngle) {
          throttle = (this.current.pieceMaxSpeed / 10);
        }

        if(this.current.angle > 58.2) {
          throttle = 0.2;
        }

        if(this.current.angle < this.before.angle && nextCurve.piece.maxSpeed > this.current.speed) {
          throttle = 1.0;
        }

        if(this.current.speed > nextCurve.piece.maxSpeed) {
          var _speed = this.current.speed,
            dif = (_speed - this.log.speeds[this.log.speeds.length-2]),
            breakingDistance = 0;

          while(_speed > nextCurve.piece.maxSpeed) {
            breakingDistance += _speed;
            tmpSpeed = _speed;
            _speed = this.physics.setCar(this).predictNextSpeed(false, tmpSpeed, dif);

            dif = (tmpSpeed - _speed);
          }

          if(breakingDistance >= nextCurve.distanceTo) {
            throttle = 0.0;
          }
        }
        
        if(this.current.pieceIndex == game.bestPiece4Turbo && this.current.angle < 40) {
          if(!this.turbo.active && this.turbo.available) {
            send({
              msgType: "turbo",
              data: "Go Bitch!"
            });
            this.turbo.active = true;
          }
        }

        if(this.current.lap == (race.config.raceSession.laps - 1) && this.current.pieceIndex >= lastRacePieceIndex-1) {
          throttle = 1.0;
        }

        if(throttle > 1) {throttle = 1.0;}
        if(throttle <= 0) {throttle = 0.0;}

        this.current.throttle = throttle;

        //game.log("current",this.current);
        //game.log("nextCurve",nextCurve);
        //game.log("nextSpeed", this.physics.setCar(this).predictNextSpeed(throttle, this.current.speed));
        console.log("");

        this.before.angle = this.current.angle;

        send({
          msgType: "throttle",
          data: throttle
        });

        if(this.log.speeds.length == 4) {
          this.drag = this.physics.setCar(this).calcDrag();
          this.mass = this.physics.setCar(this).calcMass();

          //game.log("mass", this.mass);
          //game.log("drag", this.drag);

          this.physics.speedRate = this.physics.setCar(this).calcSpeedRate();

          //game.log("speedRate",this.physics.speedRate);

          var nextCurve = this.physics.nextPieceByType("angle", 0, {index:0, distance: 0}, {index:(race.config.track.pieces.length-1), distance: 0}, 0),
            radius = race.config.track.pieces[nextCurve.index].radius;
          
          me.physics.angleSpeed = me.physics.calcSlipAngleSpeed(radius);
        }
      }
    };
    
    slotCar = function() {return new _$(arguments[0]);};
})();


var game = {
    getData: function (idName, data) {
      for(var i in data) {
        if(data[i].id.name == idName) {
          return data[i];
        }
      }
    },
    log: function (title, data) {
      title += ":";
      if(typeof data == "array" || typeof data == "object") {
        console.log(title);
        console.log(JSON.stringify(data));
      } else {
        console.log(title + data);
      }
    },
    bestPiece4Turbo: 0
  },
  race = {
    config: {},
    tick: 0,
    physics: physics()
  },
  me = slotCar();

/*************************************/

var net = require("net");
var JSONStream = require("JSONStream");

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  /*return send({
    msgType: "createRace",
    data: {
      botId: {
        name: botName,
        key: botKey
      },
      trackName: "keimola",
      password: "teste",
      carCount: 1
    }
  });*/
return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  if(json.msgType == "switchLane" || json.msgType == "turbo" || json.msgType == "throttle") {
    json.gameTick = race.tick;
  }
  
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on("data", function(data) {
  if("gameTick" in data) {
    race.tick = data.gameTick;
  }

  if (data.msgType === "carPositions") {
    var myData = game.getData(me.gameCar.name, data.data);
    
    me.log.positions.push({
      tick: race.tick,
      data: myData
    });

    me.move();
    return;
  } 

  switch(data.msgType) {
    case "yourCar" :
      me.gameCar = data.data;
      break;
    case "gameInit" :
      race.config = data.data.race;
      break;
    case "gameStart" :
      me.physics.calcBestPiece4Turbo();
      break;
    case "turboAvailable" :
      me.turbo.available = data.data;
      break;
    case "turboStart" :
      me.turbo.active = true;
      break;
    case "turboEnd" :
      me.turbo.available = me.turbo.active = false;
      break;
    case "join" :
    case "tournamentEnd" :
      send({
        msgType: "ping",
        data: {}
      });
      break;
  }

  game.log(data.msgType, data);
});

jsonStream.on("error", function() {
  return console.log("disconnected");
});
